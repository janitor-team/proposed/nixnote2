nixnote2 (2.1.6+dfsg1-1) unstable; urgency=medium

  * new upstream release 2.1.6.
  * debian/control:
    + Bump debhelper compat to v13.
  
  [ Debian Janitor ]
  * debian/upstream/metadata: Add missing metadata.

 -- Boyuan Yang <byang@debian.org>  Wed, 06 May 2020 10:24:12 -0400

nixnote2 (2.1.5+dfsg1-1) unstable; urgency=medium

  * New upstream release 2.1.5.
  * debian/control:
    + Bump Standards-Version to 4.4.1.

 -- Boyuan Yang <byang@debian.org>  Mon, 21 Oct 2019 11:33:28 -0400

nixnote2 (2.1.4+dfsg1-1) unstable; urgency=medium

  * New upstream release 2.1.4.
  * debian/copyright: Refresh information.

 -- Boyuan Yang <byang@debian.org>  Tue, 20 Aug 2019 15:32:17 -0400

nixnote2 (2.1.3+dfsg1-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/control: Bump Standards-Version to 4.4.0.

 -- Boyuan Yang <byang@debian.org>  Sun, 07 Jul 2019 12:58:00 -0400

nixnote2 (2.1.3+dfsg1-1) experimental; urgency=medium

  * New upstream release 2.1.3.
  * dfsg: Remove webpages provided in testsrc/testdata/; those HTML
    contents are taken from some random websites and they are not
    DFSG-compatible.
  * debian/watch: Apply dfsg-related watch rules.

 -- Boyuan Yang <byang@debian.org>  Mon, 06 May 2019 14:37:29 -0400

nixnote2 (2.1.2-1) unstable; urgency=medium

  * New upstream release 2.1.2.
  * debian/control:
    + Bump debhelper compat to v12.
    + Bump Standards-Version to 4.3.0.

 -- Boyuan Yang <byang@debian.org>  Wed, 02 Jan 2019 22:21:55 -0500

nixnote2 (2.1.1+git20181215-1) unstable; urgency=medium

  * New upstream git snapshot 20181215.

 -- Boyuan Yang <byang@debian.org>  Sun, 16 Dec 2018 23:54:58 -0500

nixnote2 (2.1.1+git20181211-1) unstable; urgency=medium

  * New upstream git snapshot 20181211.
    + Replace non-free icon with a GPL-3-licensed one.
      (Closes: #916219)
  * debian/patches:
    - Drop merged patches.
  * debian/copyright:
    + Update copyright for icons.

 -- Boyuan Yang <byang@debian.org>  Tue, 11 Dec 2018 16:02:23 -0500

nixnote2 (2.1.1-1) unstable; urgency=medium

  * New upstream version 2.1.1
  * debian/patches:
    - Remove merged patches.
    + Backport upstream patch to use correct QMAKE_STRIP.
      (Closes: #915590)
  * debian/maintscript:
    + Add a maintscript file to handle symlink_to_dir.
      (Closes: # 915587)

 -- Boyuan Yang <byang@debian.org>  Fri, 07 Dec 2018 22:55:04 -0500

nixnote2 (2.1.1~git20181202-1) unstable; urgency=medium

  * New upstream snapshot 2018-12-02.
  * Upload onto unstable.
  * debian/patches:
    - Drop all patches, merged upstream.
    + Add patch to install correct upstream changelog.
  * debian/README.source: Removed since it is useless.
  * debian/README.Debian: Removed, upstream is not shipping
    plugins now.
  * debian/control:
    + Set Rules-Requires-Root: no.
    + Build-depends on debhelper-compat instead of setting
      debian/compat.

 -- Boyuan Yang <byang@debian.org>  Mon, 03 Dec 2018 11:21:03 -0500

nixnote2 (2.1.0-1) experimental; urgency=medium

  * Switch to new upstream with 2.1.0 release.
  * debian/control:
    + Drop java build-dependencies.
    + Update Homepage field.
  * debian/rules:
    + Restore minimal build script.

 -- Boyuan Yang <byang@debian.org>  Mon, 26 Nov 2018 14:33:02 -0500

nixnote2 (2.0.2-4) unstable; urgency=medium

  * Rebuild against gcc 8.2.
  * debian/control:
    + Update my maintainer email address and use @debian.org one.
    + Bump Standards-Version to 4.2.1 (no changes needed).
  * debian/rules: Use "dh_missing --fail-missing".

 -- Boyuan Yang <byang@debian.org>  Wed, 07 Nov 2018 18:11:54 -0500

nixnote2 (2.0.2-3) unstable; urgency=medium

  * Backport patches from upstream trunk.
  * Use builtin evernote qt bindings again.
  * Bump Standards-Version to 4.1.4 (no changes needed).
  * Migrate to Salsa platform for Vcs fields.
  * Bump debhelper compat version to v11.

 -- Boyuan Yang <073plan@gmail.com>  Wed, 09 May 2018 21:55:09 +0800

nixnote2 (2.0.2-2) unstable; urgency=high

  * Add missing dependency libqt5sql5-sqlite. (Closes: #878185)
  * Bump Standards-Version to 4.1.1.

 -- Boyuan Yang <073plan@gmail.com>  Thu, 12 Oct 2017 10:19:07 +0800

nixnote2 (2.0.2-1) unstable; urgency=medium

  * New upstream release.
    + Force UTF-8 encoding by default. (Closes: #864978)
  * Refresh patches.
  * Bump Standard-Version to 4.1.0.

 -- Boyuan Yang <073plan@gmail.com>  Tue, 26 Sep 2017 10:59:29 +0800

nixnote2 (2.0-1) unstable; urgency=medium

  * Upload to unstable.

 -- Boyuan Yang <073plan@gmail.com>  Fri, 23 Jun 2017 22:57:30 +0800

nixnote2 (2.0-1~exp1) experimental; urgency=medium

  * New upstream stable release.
  * Update Homepage URL to use nixnote.org.
  * Refresh d/copyright information.
  * Refresh patches:
    + Drop old patches 0002-0004, applied upstream.
    + Cherry-pick upstream patch for command line import.
    + Cherry-pick upstream patch for plugin-path finding.

 -- Boyuan Yang <073plan@gmail.com>  Mon, 08 May 2017 15:24:22 +0800

nixnote2 (2.0~beta11-1) unstable; urgency=medium

  * New upstream release.
  * Switch back to build with Qt5 as upstream recommends.
    - Refresh patch about qevercloud library.
  * No longer mark as +dfsg, all files are dfsg compatible.
    - Drop old patch 0001 for README.txt, upstream has refreshed it.
    - Drop old patch 0003 for purple-theme, licensed under GPL-2+.
    - Update watch file.
    - Update README.source.
  * Cherry-pick several upstream commits.
    - Add patch 0002 for switching default notebook.
    - Add patch 0003 to fix the bug about tagging with Qt5.
    - Add patch 0004 to fix the bug about searching "-" in a note.

 -- Boyuan Yang <073plan@gmail.com>  Fri, 13 Jan 2017 11:38:04 +0800

nixnote2 (2.0~beta10+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #609849)

 -- Boyuan Yang <073plan@gmail.com>  Mon, 14 Nov 2016 16:37:20 +0800
